@extends('layou.app')
@section('content')


<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar" style="height: 11%;width: 100%;">
        <div class="container"><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><i class="fa fa-th-list"></i></button><img data-aos="fade" data-aos-delay="100" data-aos-once="true" src="/assets/img/logo.png"
                style="width: 90px;height: 70px;margin: -20px;padding: 0px;font-size: 17px;"><img src="/assets/img/EMSITE.png" style="height: 41px;width: 153px;margin: 1px 0px;padding: 0px 0px;">
            <div class="collapse navbar-collapse" id="navcol-1" style="height: 40px;width: 979.7px;">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item"><a class="nav-link" href="/start" style="width: 43px;">@lang('The.HOME')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('Contact.create')}}">@lang('The.CONTACT-US')</a></li>
                    <li class="nav-item"><a class="nav-link" href="/finish">@lang('The.OUR WORKS')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('Exhibtions.create')}}">@lang('The.LIVE EXHIBITIONS')</a></li>
                    <li class="nav-item"><a class="nav-link" href="/login">@lang('The.LOGIN')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('Merch.create')}}">@lang('The.Merch')</a></li>
                </ul>
                <div class="dropdown"><button class="btn btn-primary dropdown-toggle" aria-expanded="false" data-bs-toggle="dropdown" type="button">Language </button>
    <div class="dropdown-menu">
    <ul>
        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
           <li>
            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
             {{ $properties['native'] }}
            </a>
            </li>
        @endforeach
    </ul>
    </ul>
            </div>
    </div>
            </div>
        </div>
    </nav>

        <div class="sponsor" style="margin-top:80px;">
            <i class="text-center" style="font-size: 60px;  font-weight: bold; color: green;"> 
  <Map>

  </Map>

</i> 
            <h2 style="text-align: center ; font-weight: bold; font-size: 60px; color: teal;">@lang('The.Sponsor')</h2>
            <div class="container">
                <div class="row">
                <div class="col-4">
                    <img src="/assets/img/mcdonalds-logo.png" style="width: 100%;">
                </div>
                <div class="col-4">
                    <img src="/assets/img/mercedes.png" style="width: 100%;">                
                </div>
                <div class="col-4">
                    <img src="/assets/img/toyota-logo.png" style="width:100%">                
                </div>
                <div class="col-4">
                    <img src="/assets/img/intel-logo.png" style="width:100%">                
                </div>
                </div>
            </div>
        </div>
    </section>
    <section style="padding-top: 20px;">
        <div class="row" style="width: 98%;">
            <div class="col">
                    <div class="container-fluid" >
                        <div class="row" style="max-height: 10000px; max-width: 10000px; overflow: auto;border: 5px solid rgb(0, 0, 0);">
                       
    <div class="col" style="height: 500px; " >
        <div id="mnsry_container">

            
            @foreach($merches as $merche)
              <a href="{{route('Product.create' , ['id'=>$merche->id] )}}" target="_blank">
        <img src="/merchimages/{{$merche->Full_Name}}.jpg" style="width:100px; height: 100px"/></a>
            <span>----></span>
            
            @endforeach
            
<a href="{{route('Merch.create')}}">
    <img src="/create.png" style="width:150px; height: 150px"/>
</a>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
            </div>
            </div>

    </section>

@endsection