<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>The.Buy it  - EM-S-ITE</title>
    <link rel="stylesheet" href="/assets/css/Responsive-footer.css">
    <link rel="stylesheet" href="/assets/css/Image-Tab-Gallery-Horizontal.css">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter:400,700">
    <link rel="stylesheet" href="/assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/fonts/material-icons.min.css">
    <link rel="stylesheet" href="/assets/fonts/typicons.min.css">
    <link rel="stylesheet" href="/assets/css/--mp---Masonry-Gallery-with-Loader.css">
    <link rel="stylesheet" href="/assets/css/-Filterable-Gallery-with-Lightbox-BS4-.css">
    <link rel="stylesheet" href="/assets/css/Bootstrap-Image-Uploader.css">
    <link rel="stylesheet" href="/assets/css/Drag--Drop-Upload-Form.css">
    <link rel="stylesheet" href="/assets/css/Drag-Drop-File-Input-Upload.css">
    <link rel="stylesheet" href="/assets/css/gradient-navbar-1.css">
    <link rel="stylesheet" href="/assets/css/gradient-navbar.css">
    <link rel="stylesheet" href="/assets/css/Header-Dark.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.css">
    <link rel="stylesheet" href="/assets/css/Image_center.css">
    <link rel="stylesheet" href="/assets/css/Image-Tab-Gallery-Vertical.css">
    <link rel="stylesheet" href="/assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="/assets/css/Map-Clean.css">
    <link rel="stylesheet" href="/assets/css/Masonry-gallery-cards-responsive.css">
    <link rel="stylesheet" href="/assets/css/Panel-Element-1.css">
    <link rel="stylesheet" href="/assets/css/Panel-Element-2.css">
    <link rel="stylesheet" href="/assets/css/Panel-Element-3.css">
    <link rel="stylesheet" href="/assets/css/Panel-Element.css">
    <link rel="stylesheet" href="/assets/css/Projects-Clean.css">
    <link rel="stylesheet" href="/assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="/assets/css/Simple-Side-Nav.css">
    <link rel="stylesheet" href="/assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="/assets/css/vanilla-zoom.min.css">
    <link rel="icon" href="/assets/img/logo.png" type="image/x-icon">
</head>

<body>
    <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar" style="height: 11%;width: 100%;">
        <div class="container"><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><i class="fa fa-th-list"></i></button><img data-aos="fade" data-aos-delay="100" data-aos-once="true" src="/assets/img/logo.png" style="width: 90px;height: 70px;margin: -20px;padding: 0px;font-size: 17px;"><img src="/assets/img/EMSITE.png" style="height: 41px;width: 153px;margin: 1px 0px;padding: 0px 0px;">
            <div class="collapse navbar-collapse" id="navcol-1" style="height: 40px;width: 979.7px;">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item"><a class="nav-link" href="/start" style="width: 43px;">@lang('The.HOME')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('Contact.create')}}">@lang('The.CONTACT-US')</a></li>
                    <li class="nav-item"><a class="nav-link" href="/finish">@lang('The.OUR WORKS')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('Exhibtions.create')}}">@lang('The.LIVE EXHIBITIONS')</a></li>
                    <li class="nav-item"><a class="nav-link" href="/login">@lang('The.LOGIN')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('Merch.create')}}">@lang('The.Merch')</a></li>
                </ul>
                <div class="dropdown"><button class="btn btn-primary dropdown-toggle" aria-expanded="false" data-bs-toggle="dropdown" type="button">Language </button>
    <div class="dropdown-menu">
    <ul>
        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
           <li>
            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
             {{ $properties['native'] }}
            </a>
            </li>
        @endforeach
    </ul>
    </ul>
            </div>
    </div>
            </div>
        </div>
    </nav>
    <section style="padding-top: 4%;">
    
        <div class="container-fluid top-container">
            <div class="row">
                <div class="col">
                    <div class="img-container">
                        <img class="rounded" id="expandedImg" style="width:100%" src="/assets/img/f29a1ebaf6ba41c8871ee1ace3098555.png">
                        <div id="imgtext"></div>
                    </div>
                    @foreach($products as $product)
                    <div class="row img-row" style="padding-right: 10px;padding-left: 10px;">
                        <div class="col column">
            <img class="img-thumbnail img-fluid" src="/ProductsImage/{{$product->Model}}1.jpg" onclick="myFunction(this);" alt="image 1">
                        </div>
                        <div class="col column">
                            <img class="img-thumbnail img-fluid" src="/ProductsImage/{{ $product->Model}}2.jpg" onclick="myFunction(this);" alt="image 2">
                        </div>
                        <div class="col column">
                            <img class="img-thumbnail img-fluid" src="/ProductsImage/{{ $product->Model}}3.jpg" onclick="myFunction(this);" alt="image 3">
                        </div>
                        <div class="col column">
                            <img class="img-thumbnail img-fluid" src="/ProductsImage/{{ $product->Model}}4.jpg" onclick="myFunction(this);" alt="image 4">
                        </div>
                    </div>
                    
                </div>
                <div class="col">
                    <div class="form-group mb-3">
                        <label class="form-label">@lang('The.Brand:')</label> <span>{{$product->Brand}}</span> <br>
                        <label class="form-label">@lang('The.Model:')</label> <span>{{$product->Model}}</span> <br>
                        <label class="form-label">@lang('The.Price:')</label> <span>{{$product->Price}}</span> <br>
                        <label class="form-label">@lang('The.Warranty:')</label> <span>{{$product->Warranty}}</span> <br>  
                        
                    </div>  
                    
                </div>
                 @endforeach
                <div class="col">
                <fieldset>
                <legend><i class="fa fa-truck"></i>@lang('The.Buy Order')</legend>
                </fieldset>
<form action="{{route('Buy.store')}}" method="post">
    @csrf
                <div class="has-feedback form-group mb-3">
                <label class="form-label" for="from_name">@lang('The.Full Name')</label>
                <input type="text" class="form-control" id="from_name" tabindex="-1" name="Full_Name" required>
                </div>
                <div class="has-feedback form-group mb-3">
                <label class="form-label" for="from_name">@lang('The.Mobile Number')</label>
                <input type="tel" class="form-control" id="from_name" tabindex="-1" name="Mobile_Number" required>
                </div>
                <div class="has-feedback form-group mb-3">
                    <label class="form-label" for="from_email">@lang('The.Email')</label>
                    <input type="email" class="form-control" id="from_email" name="email" required></div>
                <div class="has-feedback form-group mb-3">
                <label class="form-label" for="from_email">@lang('The.ID Number')</label><input type="text" class="form-control" id="from_email" name="Number" required>
             </div>

            <div class="row">
             <div class="has-feedback form-group mb-3"><label class="form-label" for="from_phone">@lang('The.Address')</label>
             <div class="col">
<input type="text" class="form-control" id="from_phone" name="City" placeholder="City" /></div>
             <div class="col">
<input type="text" class="form-control" id="from_phone" name="Town" placeholder="Town" /></div>
        </div>
     <div class="form-group mb-3">
<label class="form-label" for="comments">@lang('The.Additional Notes About Product') </label>
<textarea class="form-control" id="comments" name="Comments" rows="2"></textarea>
</div>
<div class="form-group mb-3"><a href="thank-you.html">
<button class="btn btn-primary active d-block w-100" style="background-color:#16c74b;" type="submit">@lang('The.Submit') 
<i class="fa fa-chevron-circle-right"></i>
</button></a>
</div>
    </form>
                </div>
            </div>
        </div>

    </section>
    <div id="footer" class="flex-wrap">
        <div id="image-box" class="col"><img id="footer-img" src="/assets/img/unnamed.png"></div>
        <div id="end-box" class="col">
            <div id="end-content">
                <a href="/live.html"><h1 style="color: rgb(255,255,255);">@lang('The.Our works')</h1></a>
                <ul class="list-unstyled">
                    <li style="color: rgb(255,255,255);font-size: 19px;">@lang('The.Phone:')&nbsp;<a href="tel:+963932895825"><span style="color: rgb(69,73,78);">+963932895825</span></a><br></li>
                    <li style="color: rgb(255,255,255);font-size: 19px;">@lang('The.E-Mail:')&nbsp;<a href="mailto:Syrjara@gmail.com"><span style="color: rgb(69,73,78);">syrjara@gmail.com</span></a><br></li>
                </ul> <a href="/contact-us.html"> <button class="btn btn-primary" id="form-btn" type="button"><i class="fa fa-envelope-o"></i>&nbsp;@lang('The.Contact-us')</button></a>
                <div id="info-some-buttons" class="some" style="display: flex;justify-content: center;">
                    <ul class="some-list">
                        <li class="facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a></li>
                        <li class="instagram"><a href="https://www.instagram.com/"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a></li>
                        <li class="whatsapp"><a href="https://wa.link/"><i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdn.rawgit.com/digistate/resouces/master/multipleFilterMasonry.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script src="https://cdn.rawgit.com/desandro/masonry/master/dist/masonry.pkgd.min.js"></script>
    <script src="/assets/js/vanilla-zoom.js"></script>
    <script src="/assets/js/--mp---Masonry-Gallery-with-Loader.js"></script>
    <script src="/assets/js/-Filterable-Gallery-with-Lightbox-BS4-.js"></script>
    <script src="/assets/js/theme.js"></script>
    <script src="/assets/js/Bootstrap-Image-Uploader.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.js"></script>
    <script src="/assets/js/Image-Tab-Gallery-Vertical.js"></script>
    <script src="/assets/js/Panel-Element-1.js"></script>
    <script src="/assets/js/Panel-Element.js"></script>
    <script src="/assets/js/Simple-Side-Nav.js"></script>
    <script src="/assets/js/Simple-Slider.js"></script>
    <script async type="text/javascript" src="https://userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/0b51cec2d899412ea501d9269bffcda03dbf932c25e343a4b206a355923ba199.js"></script>

</body>

</html>