@extends('layou.index')


@section('content')

 <header class="pc-header ">
        <div class="header-wrapper">    
            <div class="ml-auto">
                <ul class="list-unstyled">
                    <li class="dropdown pc-h-item">
                        <a class="pc-head-link dropdown-toggle arrow-none mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="material-icons-two-tone">search</i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right pc-h-dropdown drp-search">
                            <div class="px-3">
                            <form action="{{route('Merch.index')}}" method="get">
                                <div class="form-group mb-0 d-flex align-items-center">
                                    <i data-feather="search"></i>
                                    <input type="search" name="search" class="form-control border-0 shadow-none" placeholder="Search here. . .">
                                </div>
                            </form>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown pc-h-item">
                        <a class="pc-head-link dropdown-toggle arrow-none mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span>
                                <span class="user-name">{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                                <span class="user-desc">@lang('The.Exhibitioin Admin')</span>

                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right pc-h-dropdown">
                            <div class=" dropdown-header">                          </div>

                            <a href="/login/logout" class="dropdown-item" >
                                <i class="material-icons-two-tone">chrome_reader_mode</i>
                                <span>@lang('The.Logout')</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
    </header>
    <div class="pc-container">
        <div class="pcoded-content">
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <div class="page-header-title">
                                <h5 class="m-b-10">@lang('The.Dashboard')</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="exadmin.html">@lang('The.Home')</a></li>
                                <li class="breadcrumb-item">@lang('The.Manage My Exhibitioin')</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body table-border-style">
                <div class="table-responsive">
                    <table class="table table-striped">

        <thead>
            <tr>
                <th>@lang('The.Full_Name')</th>
                <th>@lang('The.Company_Name')</th>
                
                <th>@lang('The.Exhibtion Name')</th>

                <th>@lang('The.Type_of_goods')</th>
                <th>@lang('The.Space_Required')</th>
                <th>@lang('The.image')</th>
                <th>@lang('The.action')</th>
            </tr>
        </thead>
        <tbody>

            @foreach($merchs as $merch)

            <tr>
                <td>{{ $merch->Full_Name }}</td>
                <td>{{ $merch->Company_Name }}</td>
                
               <td>{{ $merch->id_Exhibtion }}</td>

                <td>{{ $merch->Type_of_goods }}</td>
                <td>{{ $merch->Space_Required }}</td>
                <td><img src="/merchimages/{{$merch->Full_Name}}.jpg" width="60%" /></td>
                <td>
<form method="POST" action="{{ route('Merch.destroy', [$merch->id]) }}">
{{ csrf_field() }}
{{ method_field('DELETE') }}

<button class="btn btn-primary" type="submit" style="width: 85px;height: 34px;font-size: 10px;margin: -4px;background: var(--bs-red);border-color: var(--bs-green);">@lang('The.Delete')</button>
                            </form>
                </td>
                </tr>

            @endforeach
        </tbody>
    </table>
</div>
                <script src="/asset/js/plugins/bootstrap.min.js"></script>

@endsection
<!-- if('Exhibition_Name' == merch->$Exhibition_Name) -->
