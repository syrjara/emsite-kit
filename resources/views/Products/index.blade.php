<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>@lang('The.Home')</title>
    <link rel="stylesheet" href="assets/css/Responsive-footer.css">
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter:400,700">
    <link rel="stylesheet" href="/assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/fonts/material-icons.min.css">
    <link rel="stylesheet" href="/assets/fonts/typicons.min.css">
    <link rel="stylesheet" href="/assets/css/-Filterable-Gallery-with-Lightbox-BS4-.css">
    <link rel="stylesheet" href="../assets/css/Bootstrap-Image-Uploader.css">
    <link rel="stylesheet" href="../assets/css/Drag--Drop-Upload-Form.css">
    <link rel="stylesheet" href="../assets/css/Drag-Drop-File-Input-Upload.css">
    <link rel="stylesheet" href="../assets/css/gradient-navbar-1.css">
    <link rel="stylesheet" href="../assets/css/gradient-navbar.css">
    <link rel="stylesheet" href="../assets/css/Header-Dark.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.css">
    
</head>

<body>
    <nav class="navbar navbar-dark navbar-expand-md" id="app-navbar">
        <div class="container-fluid"><a class="navbar-brand" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" id="brand-logo">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M5.79166 2H1V4H4.2184L6.9872 16.6776H7V17H20V16.7519L22.1932 7.09095L22.5308 6H6.6552L6.08485 3.38852L5.79166 2ZM19.9869 8H7.092L8.62081 15H18.3978L19.9869 8Z" fill="currentColor"></path>
                    <path d="M10 22C11.1046 22 12 21.1046 12 20C12 18.8954 11.1046 18 10 18C8.89543 18 8 18.8954 8 20C8 21.1046 8.89543 22 10 22Z" fill="currentColor"></path>
                    <path d="M19 20C19 21.1046 18.1046 22 17 22C15.8954 22 15 21.1046 15 20C15 18.8954 15.8954 18 17 18C18.1046 18 19 18.8954 19 20Z" fill="currentColor"></path>
                </svg></a>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="navbar-nav ml-auto">
                        <li class="dropdown pc-h-item">
                        <a class="pc-head-link dropdown-toggle arrow-none mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span>
                            <span class="user-name">{{\Illuminate\Support\Facades\Auth::user()->name}}</span>

                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right pc-h-dropdown">
                            <div class=" dropdown-header">                          </div>

                            <a href="/login/logout" class="dropdown-item" >
                                <span>@lang('The.Logout')</span>
                            </a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <div class="modal fade" role="dialog" tabindex="-1" aria-labelledby="addsectionmodal">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    
                                    <div class="modal-body">
                                        <p>@lang('The.The content of your modal.')</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-light" type="button" data-bs-dismiss="modal">@lang('The.Close')</button>
                                        <button class="btn btn-primary" type="button">@lang('The.Save')</button></div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="dropdown"><button class="btn btn-primary dropdown-toggle" aria-expanded="false" data-bs-toggle="dropdown" type="button">Language </button>
    <div class="dropdown-menu">
    <ul>
        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
           <li>
            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
             {{ $properties['native'] }}
            </a>
            </li>
        @endforeach
    </ul>
    </ul>
            </div>
    </div>
                <div class="modal fade" role="dialog" tabindex="-1" id="addSectorModal" aria-labelledby="addSectorModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header"><i class="material-icons icon-dashboard" style="width: 66px;height: 53px;margin: -10px;padding: 5px;font-size: 40px;color: rgb(0,0,0);">@lang('The.store_mall_directory')</i>
                                <h4 style="color: rgb(0,0,0);">@lang('The.New Sector')</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-footer"><button class="btn btn-light" id="btnSaveModalSector" type="button" data-bs-dismiss="modal" style="background: var(--bs-cyan);">@lang('The.Save')</button><button class="btn btn-light" id="btnModelCloseAddDashboard-1" type="button" data-bs-dismiss="modal">@lang('The.Close')</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <section>
        <section style="width: 100%;height: 100%;">
            <div>
                <header class="header-dark" style="padding: 0px;">
                    <nav class="navbar navbar-dark navbar-expand-lg navigation-clean-search">
                        <div class="container">
                        <div class="collapse navbar-collapse" id="navcol-1">
                            
                                    <section id="amihub-elemnt-menubar">
                                    <div class="modal fade" role="dialog" tabindex="-1" id="additemModal" aria-labelledby="additemModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 style="color: rgb(0,0,0);">@lang('The.Component')</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
<form action="{{route('Product.store')}}" method="post" enctype="multipart/form-data">
 @csrf

    <div class="form-group mb-3">
    <label class="form-label">@lang('The.Brand:')</label>
    <input class="form-control" type="text" id="fieldNameDashboard" name="Brand" required>
    <label class="form-label">@lang('The.Model:')</label>
    <input class="form-control"type="text" id="fieldNameDashboard-2" name="Model" required>
    <label class="form-label">@lang('The.Price:')</label>
    <input class="form-control" type="text" id="fieldNameDashboard-3" name="Price" required><br>
    <label class="form-label">@lang('The.Warranty:')</label>
    <input class="form-control" type="text" id="fieldNameDashboard-4" name="Warranty" required><br>
    <label class="form-label">@lang('The.Description:')</label>
    <input type="text" class="form-control" style="width: 100%;height: 90px;" name="Description" required>
    <div class="files color form-group mb-3">
    <label class="form-label" style="color: var(--bs-green);">
    <strong>@lang('The.Upload Photos for your Item: (4 Photos)')</strong>
    </label>
    <div>

    <input type="file" class="form-control" name="image1" required>
    </div>
    <div>
    <input type="file" class="form-control" name="image2" required> 
</div>

    <div>
    <input type="file" class="form-control" name="image3" required>    </div>

    <div>
    <input type="file" class="form-control" name="image4" required />
    </div>
    </div>
   <br>
      <input type="submit" value="ADD" class="btn btn-light" style="background: var(--bs-cyan" >


</form>
</div>

 </div>
  <div class="modal-footer">
   <button class="btn btn-light" id="btnModelCloseAddDashboard" style="background: var(--bs-cyan" type="button" data-bs-dismiss="modal">Close</button>
 </div>
 </div>
             </div>
            </div>
            <button class="btn btn-primary btn btn-outline-info btn-block" type="button" data-bs-toggle="modal" data-bs-target="#additemModal" style="color: rgb(255,255,255);font-size: 16px; "><i class="fa fa-plus me-2"></i>&nbsp;@lang('The.Add Item')
            </button>
                                </section>
                            </div>
                        </div>
                    </nav>
                    <div class="row" style="width: 99%;margin: 0px 1px;">
                        <h1 class="text-center" style="color: rgb(255,255,255);">ِ{{ $merch->Company_Name }}</h1>

                        @foreach($products as $product)
                        <section id="container" class="col" style="width: 35%;">
                            <div class="col-md-6" style="padding: 10px;margin: 0px 5px;width: 400px;">
                                    <div class="card device-card" style="width: 100%;">
                                        <div class="card-body" style="width: 350px; height: 120px;margin: 0px;padding: 16px;">
                                            <span class="card-title"><strong>@lang('The.Name :')</strong></span><span>{{$product->Brand}}</span><br>
                                            <span class="card-text" style="margin: 0px;height: 30px;font-size: 15px;">@lang('The.Price : ')</span> <span>{{$product->Price}}</span>
                                        </div>
                                        <div class="container">
                                            <div class="row">
<div class="col">
<div>
<i class="fa fa-remove" style="width: 75px;height: 20px;margin: 5px;font-size: 35px;">
    
</i>
<form method="POST" action="{{ route('Product.destroy', [$product->id]) }}">
{{ csrf_field() }}
{{ method_field('DELETE') }}

<button class="btn btn-primary" type="submit" style="width: 80px;height: 30px;font-size: 10px;margin: -4px;background: var(--bs-red);border-color: var(--bs-green);">@lang('The.Delete')</button>
                            </form>
</div>
            </div>
            <div class="col">
             <div>
                <i class="fa fa-edit" style="width: 75px;height: 20px;margin: 5px;font-size: 35px;"></i>
            </div>
            <a href="{{route('Product.edit',$product->id)}}">
            <button class="btn btn-primary" type="button" style="width: 80px;height: 30px;font-size: 10px;margin: -4px;background: var(--bs-yellow);border-color: var(--bs-green);">@lang('The.Edit')
            </button></a>
                </div>
                                                
            </div>
         </div>
        </div>
         </div>

    </section>
 @endforeach
    </div>
</header>
</div>
</section>
    </section>


    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdn.rawgit.com/digistate/resouces/master/multipleFilterMasonry.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script src="https://cdn.rawgit.com/desandro/masonry/master/dist/masonry.pkgd.min.js"></script>
    <script src="../assets/js/vanilla-zoom.js"></script>
    <script src="../assets/js/--mp---Masonry-Gallery-with-Loader.js"></script>
    <script src="../assets/js/-Filterable-Gallery-with-Lightbox-BS4-.js"></script>
    <script src="../assets/js/theme.js"></script>
    <script src="../assets/js/Bootstrap-Image-Uploader.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.js"></script>
    <script src="../assets/js/Image-Tab-Gallery-Vertical.js"></script>
    <script src="../assets/js/Panel-Element-1.js"></script>
    <script src="../assets/js/Panel-Element.js"></script>
    <script src="../assets/js/Simple-Side-Nav.js"></script>
    <script src="../assets/js/Simple-Slider.js"></script>
    <script async type="text/javascript" src="https://userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/0b51cec2d899412ea501d9269bffcda03dbf932c25e343a4b206a355923ba199.js"></script>
<script src="/asset/js/pages/dashboard-sale.js"></script>
 <script src="/asset/js/vendor-all.min.js"></script>
    <script src="/asset/js/plugins/bootstrap.min.js"></script>
    <script src="/asset/js/plugins/feather.min.js"></script>
    <script src="/asset/js/pcoded.min.js"></script>

<script src="/asset/js/plugins/apexcharts.min.js"></script>

<script src="/asset/js/pages/dashboard-sale.js"></script>   
</body>

</html>
