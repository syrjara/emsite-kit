@extends('layou.app')
@section('content')

    <section class="border rounded-pill login-clean" style="background: url(&quot;/assets/img/exhibitions-gl-events.jpg&quot;);">
        <form data-aos="zoom-in" data-aos-duration="500" data-aos-delay="50" action="{{route('Merch.store')}}" method="post" style="height: 60%;" enctype="multipart/form-data">
            @csrf
            <div class="illustration">
                <img src="/assets/img/f29a1ebaf6ba41c8871ee1ace3098555.png">
            </div>

            <label>Choose Exhibition</label>
           <select class="form-select" name="id_Exhibtion">
            @foreach($merches as $exhibtioins)
            @if($exhibtioins->state == 'active')
            <option name="id_Exhibtion">{{$exhibtioins->id}}.{{ $exhibtioins->Exhibition_Name }}</option>
            @endif
            @endforeach
          </select>

            <div class="mb-3">
                <input class="form-control form-control-lg" type="text" name="Full_Name" placeholder="Full_Name" style="padding-top: 0px;">
            </div>

            <div>
                <input class="form-control form-control-lg" type="text" name="Company_Name" placeholder="Company_Name" style="padding-top: 0px;"  required>
            </div>

            <div class="mb-3">
                <input class="form-control" type="email" name="Email" placeholder="Email" style="padding-top: 0px;" required>
            </div>

            <div class="mb-3">
                <input class="form-control" type="password" name="Password" placeholder="Password" style="padding-top: 0px;" required>
            </div>

            <div>
                <input class="form-control form-control-lg" type="text" name="Type_of_goods" placeholder="Type_of_goods" required>
            </div>

            <div>
                <select class="form-select" name="Space_Required">
                    <optgroup label="Space_Required">
                        <option name="4 x 4">4 x 4</option>
                        <option name="10 x 10">10 x 10</option>
                        <option name="50 x 50">50 x 50</option>
                    </optgroup>
                </select>
            </div>

            <div class="files color form-group mb-3"><label class="form-label" style="color: var(--bs-green);">
                <strong> @lang('The.Upload Company logo') </strong>
            </label>
            <input type="file" name="image" class="form-control" multiple accept=".png, .jpg, .jpeg" style="width: auto;height: auto;margin: 0px;padding: 121px 0px 85px 118.1px;"
                />
            </div>
            <div class="mb-3">
                <button class="btn btn-primary d-block w-100" type="submit" style="background: var(--bs-green);">@lang('The.SignUp and upload!')</button>
            
        </form>
    </section>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdn.rawgit.com/digistate/resouces/master/multipleFilterMasonry.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script src="https://cdn.rawgit.com/desandro/masonry/master/dist/masonry.pkgd.min.js"></script>
    <script src="/assets/js/vanilla-zoom.js"></script>
    <script src="/assets/js/--mp---Masonry-Gallery-with-Loader.js"></script>
    <script src="/assets/js/-Filterable-Gallery-with-Lightbox-BS4-.js"></script>
    <script src="/assets/js/theme.js"></script>
    <script src="/assets/js/Bootstrap-Image-Uploader.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.js"></script>
    <script src="/assets/js/Image-Tab-Gallery-Vertical.js"></script>
    <script src="/assets/js/Panel-Element-1.js"></script>
    <script src="/assets/js/Panel-Element.js"></script>
    <script src="/assets/js/Simple-Side-Nav.js"></script>
    <script src="/assets/js/Simple-Slider.js"></script>
    <script async type="text/javascript" src="https://userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/0b51cec2d899412ea501d9269bffcda03dbf932c25e343a4b206a355923ba199.js"></script>

@endsection
