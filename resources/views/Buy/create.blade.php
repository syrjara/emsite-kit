@extends('layou.index')

@section('content')
<header class="pc-header ">
        <div class="header-wrapper">    
            <div class="ml-auto">
                <ul class="list-unstyled">
                    <li class="dropdown pc-h-item">
                        <a class="pc-head-link dropdown-toggle arrow-none mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="material-icons-two-tone">search</i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right pc-h-dropdown drp-search">
                            <div class="px-3">
                            <form action="{{route('Buy.create')}}" method="get">
                                <div class="form-group mb-0 d-flex align-items-center">
                                    <i data-feather="search"></i>
                                    <input type="search" name="search" class="form-control border-0 shadow-none" placeholder="Search here. . .">
                                </div>
                            </form>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown pc-h-item">
                        <a class="pc-head-link dropdown-toggle arrow-none mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span>
                                <span class="user-name">{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                                <span class="user-desc">@lang('The.Exhibitioin Admin')</span>

                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right pc-h-dropdown">
                            <div class=" dropdown-header">                          </div>

                            <a href="/login/logout" class="dropdown-item" >
                                <i class="material-icons-two-tone">chrome_reader_mode</i>
                                <span>@lang('The.Logout')</span>
                            </a>
                        </div>
                    </li>
                </ul>
                
            </div>

        </div>
    </header>
    <div class="pc-container">
        <div class="pcoded-content">
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <div class="page-header-title">
                                <h5 class="m-b-10">@lang('The.Dashboard')</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="exadmin.html">@lang('The.Home')</a></li>
                                <li class="breadcrumb-item">@lang('The.Manage My Exhibitioin')</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body table-border-style">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            
                            <th>@lang('The.Full Name')</th>
                            <th>@lang('The.Mobile Number')</th>
                            <th>@lang('The.email')</th>
                            <th>@lang('The.Number')</th>
                            <th>@lang('The.City')</th>
                            <th>@lang('The.Town')</th>
                            <th>@lang('The.Comments')</th>
                            <th>@lang('The.action')</th>

                        </thead>
                        <tbody>
                @foreach($buy as $by )
                    <tr>
                        <td>{{ $by->Full_Name }}</td>
                        <td>{{ $by->Mobile_Number }}</td>
                        <td>{{ $by->email }}</td>
                        <td>{{ $by->Number }}</td>
                        <td>{{ $by->City }}</td>
                        <td>{{ $by->Town }}</td>
                        <td>{{ $by->Comments }}</td>
                        <td>
                            <form method="POST" action="{{ route('Buy.destroy', [$by->id]) }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit">@lang('The.Delete')</button>
                            </form>
                        </td>
                    </tr>     
            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
            
    <script src="/asset/js/vendor-all.min.js"></script>
    <script src="/asset/js/plugins/bootstrap.min.js"></script>
    <script src="/asset/js/plugins/feather.min.js"></script>
    <script src="/asset/js/pcoded.min.js"></script>

<script src="/asset/js/plugins/apexcharts.min.js"></script>

<script src="/asset/js/pages/dashboard-sale.js"></script>


@endsection
