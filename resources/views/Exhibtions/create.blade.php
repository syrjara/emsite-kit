@extends('layou.app')
@section('content')
 <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar" style="height: 11%;width: 100%;">
        <div class="container"><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><i class="fa fa-th-list"></i></button><img data-aos="fade" data-aos-delay="100" data-aos-once="true" src="/assets/img/logo.png"
                style="width: 90px;height: 70px;margin: -20px;padding: 0px;font-size: 17px;"><img src="/assets/img/EMSITE.png" style="height: 41px;width: 153px;margin: 1px 0px;padding: 0px 0px;">
            <div class="collapse navbar-collapse" id="navcol-1" style="height: 40px;width: 979.7px;">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item"><a class="nav-link" href="/start" style="width: 43px;">@lang('The.HOME')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('Contact.create')}}">@lang('The.CONTACT-US')</a></li>
                    <li class="nav-item"><a class="nav-link" href="/finish">@lang('The.OUR WORKS')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('Exhibtions.create')}}">@lang('The.LIVE EXHIBITIONS')</a></li>
                    <li class="nav-item"><a class="nav-link" href="/login">@lang('The.LOGIN')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('Merch.create')}}">@lang('The.Merch')</a></li>
                    <li>
  </li>
</ul>
<div class="dropdown"><button class="btn btn-primary dropdown-toggle" aria-expanded="false" data-bs-toggle="dropdown" type="button">Language </button>
    <div class="dropdown-menu">
    <ul>
        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
           <li>
            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
             {{ $properties['native'] }}
            </a>
            </li>
        @endforeach
    </ul>
    </ul>
            </div>
    </div>
</div>
                            <div class="px-3">
                            <form action="{{route('Exhibtions.create')}}" method="get">
                                <div class="form-group mb-0 d-flex align-items-center">
                                    <i data-feather="search"></i>
                                    <input type="search" name="search" class="form-control border-0 shadow-none" placeholder="Search here. . .">
                                </div>
                            </form>
                            </div>
                        
</form>
</li>
                </ul>
            </div>
        </div>
    </nav>
    <section class="projects-clean" style="height: 1070px;margin: 52px;">
        <div class="container">
            <div class="intro">
                <h2 class="text-center" style="color: var(--bs-green);">@lang('The.Live Exhibition')<i class="material-icons" style="font-size: 42px;color: var(--bs-red);margin: 1px;">live_tv</i></h2>
                <p class="text-center"> @lang('The.Browse the following exhibitions and book your purchases or visit and buy yourself') </p>
            </div>
            <div class="row Exhibition">
            @foreach($exhibtions as $exhibtion)
            @if($exhibtion->state == 'active')
                <div class="col item">
                    <a href="{{route('Exhibtions.index', ['id' => $exhibtion->id])}}"> 
                        <img class="img-fluid" src="/Exhibition/{{$exhibtion->Exhibition_Name}}.jpg"></a>

                        <h3 class="name">{{$exhibtion->Exhibition_Name}}   Exhibition</h3><span></span>
                    <h5>Date </h5> <span>{{$exhibtion->created_at}}</span>               
            </div>
            @endif
            @endforeach
        </div>
    </section>

    
             
    </div>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdn.rawgit.com/digistate/resouces/master/multipleFilterMasonry.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script src="https://cdn.rawgit.com/desandro/masonry/master/dist/masonry.pkgd.min.js"></script>
    <script src="/assets/js/vanilla-zoom.js"></script>
    <script src="/assets/js/--mp---Masonry-Gallery-with-Loader.js"></script>
    <script src="/assets/js/-Filterable-Gallery-with-Lightbox-BS4-.js"></script>
    <script src="/assets/js/theme.js"></script>
    <script src="/assets/js/Bootstrap-Image-Uploader.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.js"></script>
    <script src="/assets/js/Image-Tab-Gallery-Vertical.js"></script>
    <script src="/assets/js/Panel-Element-1.js"></script>
    <script src="/assets/js/Panel-Element.js"></script>
    <script src="/assets/js/Simple-Side-Nav.js"></script>
    <script src="/assets/js/Simple-Slider.js"></script>
    <script async type="text/javascript" src="https://userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/0b51cec2d899412ea501d9269bffcda03dbf932c25e343a4b206a355923ba199.js"></script>

@endsection