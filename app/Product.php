<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['Brand', 'Model', 'Price', 'Warranty', 'Description','merch_id' ,'image1', 'image2','image3','image4'];
}
