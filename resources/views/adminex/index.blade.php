@extends('layou.index')

@section('content')
<header class="pc-header ">
        <div class="header-wrapper">    
            <div class="ml-auto">
                <ul class="list-unstyled">
                    <li class="dropdown pc-h-item">
                        <a class="pc-head-link dropdown-toggle arrow-none mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="material-icons-two-tone">search</i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right pc-h-dropdown drp-search">
                            <div class="px-3">
                            <form action="{{route('Adminex.index')}}" method="get">
                                <div class="form-group mb-0 d-flex align-items-center">
                                    <i data-feather="search"></i>
                                    <input type="search" name="search" class="form-control border-0 shadow-none" placeholder="Search here. . .">
                                </div>
                            </form>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown pc-h-item">
                        <a class="pc-head-link dropdown-toggle arrow-none mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <span>
                                <span class="user-name">{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                                <span class="user-desc">@lang('The.Exhibitioin Admin')</span>

                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right pc-h-dropdown">
                            <div class=" dropdown-header">                          </div>

                            <a href="/login/logout" class="dropdown-item" >
                                <i class="material-icons-two-tone">chrome_reader_mode</i>
                                <span>@lang('The.Logout')</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
    </header>
    <div class="pc-container">
        <div class="pcoded-content">
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <div class="page-header-title">
                                <h5 class="m-b-10">@lang('The.Dashboard')</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="exadmin.html">@lang('The.Home')</a></li>
                                <li class="breadcrumb-item">@lang('The.Manage My Exhibitioin')</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body table-border-style">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <th>@lang('The.Exhibition Name')</th>
                            <th>@lang('The.Type Exhibition')</th>
                            <th>@lang('The.Attendees Number')</th>
                            <th>@lang('The.Companies')</th>
                            <th>@lang('The.Event Budget')</th>
                            <th>@lang('The.States')</th>
                            <th>@lang('The.action')</th>
                        </thead>
                        <tbody>
                @foreach($exhibtions as $exhibtion )
                    <tr>
                        <td>{{ $exhibtion->Exhibition_Name }}</td>
                        <td>{{ $exhibtion->Type_of_Exhibition }}</td>
                        <td>{{ $exhibtion->Number_of_Attendees }}</td>
                        <td>{{ $exhibtion->Number_of_Exhibiting_Companies }}</td>
                        <td>{{ $exhibtion->Event_Budget }}</td>
                        <td>{{ $exhibtion->state }}</td>
                        <td>
                             <a href="{{route('Adminex.edit',$exhibtion->id)}}" style="width: 85px;height: 34px;font-size: 21px;margin: -4px;background: var(--bs-red);border-color: var(--bs-blue);">@lang('The.Edit')</a>
                            <form method="POST" action="{{ route('Adminex.destroy', [$exhibtion->id]) }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button  class="btn btn-primary" type="submit" style="width: 85px;height: 34px;font-size: 10px;margin: -4px;background: var(--bs-red);border-color: var(--bs-green);">@lang('The.Delete')</button>
                            </form>
                        </td>
                    </tr>     
            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
            
    <script src="/asset/js/vendor-all.min.js"></script>
    <script src="/asset/js/plugins/bootstrap.min.js"></script>
    <script src="/asset/js/plugins/feather.min.js"></script>
    <script src="/asset/js/pcoded.min.js"></script>

<script src="/asset/js/plugins/apexcharts.min.js"></script>

<script src="/asset/js/pages/dashboard-sale.js"></script>


@endsection
