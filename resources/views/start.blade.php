<!DOCTYPE html>
<html style="width: 100%;">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Home - EM-S-ITE</title>
    <link rel="stylesheet" href="/assets/css/Responsive-footer.css">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter:400,700">
    <link rel="stylesheet" href="/assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/fonts/material-icons.min.css">
    <link rel="stylesheet" href="/assets/fonts/typicons.min.css">
    <link rel="stylesheet" href="/assets/css/--mp---Masonry-Gallery-with-Loader.css">
    <link rel="stylesheet" href="/assets/css/-Filterable-Gallery-with-Lightbox-BS4-.css">
    <link rel="stylesheet" href="/assets/css/Bootstrap-Image-Uploader.css">
    <link rel="stylesheet" href="/assets/css/Drag--Drop-Upload-Form.css">
    <link rel="stylesheet" href="/assets/css/Drag-Drop-File-Input-Upload.css">
    <link rel="stylesheet" href="/assets/css/gradient-navbar-1.css">
    <link rel="stylesheet" href="/assets/css/gradient-navbar.css">
    <link rel="stylesheet" href="/assets/css/Header-Dark.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.css">
    <link rel="stylesheet" href="/assets/css/Image_center.css">
    <link rel="stylesheet" href="/assets/css/Image-Tab-Gallery-Vertical.css">
    <link rel="stylesheet" href="/assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="/assets/css/Map-Clean.css">
    <link rel="stylesheet" href="/assets/css/Masonry-gallery-cards-responsive.css">
    <link rel="stylesheet" href="/assets/css/Panel-Element-1.css">
    <link rel="stylesheet" href="/assets/css/Panel-Element-2.css">
    <link rel="stylesheet" href="/assets/css/Panel-Element-3.css">
    <link rel="stylesheet" href="/assets/css/Panel-Element.css">
    <link rel="stylesheet" href="/assets/css/Projects-Clean.css">
    <link rel="stylesheet" href="/assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="/assets/css/Simple-Side-Nav.css">
    <link rel="stylesheet" href="/assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="/assets/css/vanilla-zoom.min.css">
    <link rel="icon" href="/assets/img/logo.png" type="image/x-icon">

</head>

<body style="width: 100%;">
   @extends('layou.app')
@section('content')
    <main class="page landing-page" style="width: 100%;">
        <section data-bss-parallax-bg="true" class="clean-block clean-hero" style="color: rgba(0,0,0,0.85);background: url(&quot;/assets/img/exhibitions-gl-events.jpg&quot;) center / auto no-repeat;border-color: var(--bs-white);filter: brightness(106%);background-image: url(/assets/img/exhibitions-gl-events.jpg);background-position: center;background-size: cover;width: 100%;">
            <div class="text">
                <h2 style="font-size: 41.128px;">@lang('The.Exhibition Managment System ITE')</h2>
                <p style="font-size: 22px;"><em>@lang('The.Event organizers and exhibitors from all over the world come to us for all of their experiential marketing needs.')</em><br></p>
            </div>
        </section>
        <section style="width: 95%;padding-top: 1%;padding-left: 5%;">
            <div>
                <div class="row" style="height: 310px;">
                    <div class="col">
                        <div class="carousel slide" data-bs-ride="carousel" id="carousel-1" style="width: 400.5px;">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                  <img class="w-100 d-block" src="/assets/img/20170905_SMARTOE_006-1024x683.jpg" alt="Slide Image">
                                </div>
                                <div class="carousel-item">
                                  <img class="w-100 d-block" src="/assets/img/d1b265d813c177ef142e01da625790f8.jpg" alt="Slide Image">
                                </div>
                                <div class="carousel-item">
                                <img class="w-100 d-block" src="/assets/img/download.jpg" alt="Slide Image">
                                </div>
                            </div>
                            <div><a class="carousel-control-prev" href="#carousel-1" role="button" data-bs-slide="prev">
                              <span class="carousel-control-prev-icon">
                            </span>
                              <span class="visually-hidden">@lang('The.Previous')</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-1" role="button" data-bs-slide="next"><span class="carousel-control-next-icon">
                            </span>
                            <span class="visually-hidden">@lang('The.Next')</span>
                            </a>
                          </div>
                            <ol class="carousel-indicators">
                                <li data-bs-target="#carousel-1" data-bs-slide-to="0" class="active"></li>
                                <li data-bs-target="#carousel-1" data-bs-slide-to="1"></li>
                                <li data-bs-target="#carousel-1" data-bs-slide-to="2"></li>
                            </ol>
                        </div>
                    </div>
                    <div class="col">
                        <p style="font-size: 22px;color: var(--bs-success);"><br>▶<strong>@lang('The.Browse exhibition and book your purchases').</strong><br><br>▶<strong>@lang('The.View monitor the customer journey in one place')</strong><br><br>▶<strong>@lang('The.Import, Design, and Show Booths in Exhibition Floor Plan, Then Share It With the World')</strong><br><br><strong>▶ @lang('The.Market your merchandise')</strong></br></p>
                    </div>
                </div>
            </div>
        </section>
        <section class="clean-block clean-info dark" style="width: 95%;background: rgb(255,255,255);padding-top: 1%;padding-left: 5%;padding-bottom: 0px;">
            
            <!-- Styles -->
<style>
    #chartdiv {
      width: 100%;
      height: 500px;
    }
    </style>
    
    <!-- Resources -->
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
    
    <!-- Chart code -->
    <script>
    am4core.ready(function() {
    
    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end
    
    var chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
    
    chart.data = [
      {
        country: "Damascus Exhibition",
        visits: 1000
      },
      {
        country: "Aleppo",
        visits: 800
      },
      {
        country: "ITE",
        visits: 500
      },
      {
        country: "Basel",
        visits: 200
      },
      
    ];
    
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.dataFields.category = "country";
    categoryAxis.renderer.minGridDistance = 40;
    categoryAxis.fontSize = 11;
    
    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.min = 0;
    valueAxis.max = 1200;
    valueAxis.strictMinMax = true;
    valueAxis.renderer.minGridDistance = 30;

    var axisBreak = valueAxis.axisBreaks.create();
    axisBreak.startValue = 2100;
    axisBreak.endValue = 22900;

    var d = (axisBreak.endValue - axisBreak.startValue) / (valueAxis.max - valueAxis.min);
    axisBreak.breakSize = 0.05 * (1 - d) / d;
    
    var hoverState = axisBreak.states.create("hover");
    hoverState.properties.breakSize = 1;
    hoverState.properties.opacity = 0.1;
    hoverState.transitionDuration = 1500;
    
    axisBreak.defaultState.transitionDuration = 1000;
    
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.categoryX = "country";
    series.dataFields.valueY = "visits";
    series.columns.template.tooltipText = "{valueY.value}";
    series.columns.template.tooltipY = 0;
    series.columns.template.strokeOpacity = 0;
    series.columns.template.adapter.add("fill", function(fill, target) {
      return chart.colors.getIndex(target.dataItem.index);
    });
    
    });
    </script>
    
    <div id="chartdiv"></div>
        </section>
    </main>
    
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdn.rawgit.com/digistate/resouces/master/multipleFilterMasonry.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script src="https://cdn.rawgit.com/desandro/masonry/master/dist/masonry.pkgd.min.js"></script>
    <script src="/assets/js/vanilla-zoom.js"></script>
    <script src="/assets/js/--mp---Masonry-Gallery-with-Loader.js"></script>
    <script src="/assets/js/-Filterable-Gallery-with-Lightbox-BS4-.js"></script>
    <script src="/assets/js/theme.js"></script>
    <script src="/assets/js/Bootstrap-Image-Uploader.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.js"></script>
    <script src="/assets/js/Image-Tab-Gallery-Vertical.js"></script>
    <script src="/assets/js/Panel-Element-1.js"></script>
    <script src="/assets/js/Panel-Element.js"></script>
    <script src="/assets/js/selectex.js"></script>
    <script src="/assets/js/Simple-Side-Nav.js"></script>
    <script src="/assets/js/Simple-Slider.js"></script>
    <script async type="text/javascript" src="https://userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/0b51cec2d899412ea501d9269bffcda03dbf932c25e343a4b206a355923ba199.js"></script>
</body>

@endsection