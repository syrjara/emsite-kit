<!DOCTYPE html>
<html lang="en">

<head>
    <title>Dashboard Exibition Manage</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="DashboardKit is made using Bootstrap 5 design framework. Download the free admin template & use it for your project.">
    <meta name="keywords" content="DashboardKit, Dashboard Kit, Dashboard UI Kit, Bootstrap 5, Admin Template, Admin Dashboard, CRM, CMS, Free Bootstrap Admin Template">
    <meta name="author" content="DashboardKit ">


    <link rel="icon" href="/asset/images/logo.png" type="image/x-icon">

    <link rel="stylesheet" href="/asset/fonts/feather.css">
    <link rel="stylesheet" href="/asset/fonts/fontawesome.css">
    <link rel="stylesheet" href="/asset/fonts/material.css">

    <link rel="stylesheet" href="/asset/css/style.css" id="main-style-link">

</head>
<body>
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <div class="pc-mob-header pc-header">
        <div class="pcm-logo">
            <img src="/asset/images/logo.png" style="width: 75px;" class="logo logo-lg"> @lang('The.Dashboard')
        </div>
        <div class="pcm-toolbar">
            <a href="#!" class="pc-head-link" id="mobile-collapse">
                <div class="hamburger hamburger--arrowturn">
                    <div class="hamburger-box">
                        <div class="hamburger-inner"></div>
                    </div>
                </div>
            </a>
            <a href="#!" class="pc-head-link" id="headerdrp-collapse">
                <i data-feather="align-right"></i>
            </a>
            <a href="#!" class="pc-head-link" id="header-collapse">
                <i data-feather="more-vertical"></i>
            </a>
        </div>
    </div>

    <nav class="pc-sidebar ">
        <div class="navbar-wrapper">
            <div class="m-header">
                <a href="/start" class="b-brand">
                    <img src="/asset/images/logo.png" style="width: 75px;" class="logo logo-lg">@lang('The.Dashboard Exhibitioin')
                    <img src="/asset/images/logo.png" alt="" class="logo logo-sm">
                </a>
            </div>
            <div class="navbar-content">
                <ul class="pc-navbar">
                    
                    <li class="pc-item">
                        <a href="{{route('Adminex.create')}}" class="pc-link ">
                            <span class="pc-micon">
                                <i class="material-icons-two-tone">add</i>
                            </span>
                            <span class="pc-mtext">@lang('The.Create')</span>
                        </a>
                    </li>

                    <li class="pc-item">
                        <a href="{{route('Adminex.index')}}" class="pc-link "><span class="pc-micon"><i class="material-icons-two-tone">settings</i>
                        </span>
                        <span class="pc-mtext">@lang('The.Manage')</span>
                    </a>
                    </li>

                    <li class="pc-item">
                        <a href="{{route('Merch.index')}}" class="pc-link "><span class="pc-micon"><i class="material-icons-two-tone">person</i></span><span class="pc-mtext">@lang('The.Merches')</span></a>
                    </li>

                    <li class="pc-item">
                        <a href="{{route('Contact.index')}}" class="pc-link "><span class="pc-micon"><i class="material-icons-two-tone">messenger</i></span><span class="pc-mtext">@lang('The.Contact')</span></a>
                    </li>
                    <li class="pc-item">
                        <a href="{{route('Buy.create')}}" class="pc-link "><span class="pc-micon"><i class="material-icons-two-tone">shop</i></span><span class="pc-mtext">@lang('The.Buy orders')</span></a>
                    </li>
                </ul>

            </div>

        </div>
        
    </nav>

    
<script src="/asset/js/vendor-all.min.js"></script>
    <script src="/asset/js/plugins/bootstrap.min.js"></script>
    <script src="/asset/js/plugins/feather.min.js"></script>
    <script src="/asset/js/pcoded.min.js"></script>

<script src="/asset/js/plugins/apexcharts.min.js"></script>

@yield('content')
<script src="/asset/js/pages/dashboard-sale.js"></script>
 <script src="/asset/js/vendor-all.min.js"></script>
    <script src="/asset/js/plugins/bootstrap.min.js"></script>
    <script src="/asset/js/plugins/feather.min.js"></script>
    <script src="/asset/js/pcoded.min.js"></script>

<script src="/asset/js/plugins/apexcharts.min.js"></script>

<script src="/asset/js/pages/dashboard-sale.js"></script>
</body>

</html>