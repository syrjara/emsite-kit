@extends('layou.app')
@section('content')

    <section class="projects-clean" style="height: 1070px;margin: 52px;">
        <div class="container">
            <div class="intro">
                <h2 class="text-center" style="color: var(--bs-green);">@lang('The.Finished Exhibition')<i class="material-icons" style="font-size: 42px;color: var(--bs-red);margin: 1px;">timer_off</i></h2>
                <p class="text-center">@lang('The.Browse Expired Exhibitions')</p>
            </div>
            <div class="row Exhibition">
            @foreach($exhibtions as $exhibtion)
            @if($exhibtion->state == 'finish')
                <div class="col item">
                    <a href="{{route('Exhibtions.index', ['id' => $exhibtion->id])}}"> 
                        <img class="img-fluid" src="/Exhibition/{{$exhibtion->Exhibition_Name}}.jpg"></a>

                        <h3 class="name">{{$exhibtion->Exhibition_Name}}   Exhibition</h3><span></span>
                    <h5>@lang('The.Date') </h5> <span>{{$exhibtion->created_at}}</span>               
            </div>
            @endif
            @endforeach
        </div>
    </section>

    
             
    </div>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdn.rawgit.com/digistate/resouces/master/multipleFilterMasonry.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script src="https://cdn.rawgit.com/desandro/masonry/master/dist/masonry.pkgd.min.js"></script>
    <script src="/assets/js/vanilla-zoom.js"></script>
    <script src="/assets/js/--mp---Masonry-Gallery-with-Loader.js"></script>
    <script src="/assets/js/-Filterable-Gallery-with-Lightbox-BS4-.js"></script>
    <script src="/assets/js/theme.js"></script>
    <script src="/assets/js/Bootstrap-Image-Uploader.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.js"></script>
    <script src="/assets/js/Image-Tab-Gallery-Vertical.js"></script>
    <script src="/assets/js/Panel-Element-1.js"></script>
    <script src="/assets/js/Panel-Element.js"></script>
    <script src="/assets/js/Simple-Side-Nav.js"></script>
    <script src="/assets/js/Simple-Slider.js"></script>
    <script async type="text/javascript" src="https://userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/0b51cec2d899412ea501d9269bffcda03dbf932c25e343a4b206a355923ba199.js"></script>

@endsection