<?php

namespace App\Http\Controllers;

use App\Adminex;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Cache;

class AdminexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if($request->search){
        
        $search = $request->get('search');
        $exhibtions = DB::table('adminexes')->where('Exhibition_Name', 'like' , '%' . $search . '%')->paginate(5);
        return view('Adminex.index',['exhibtions'=>$exhibtions]);
        }

        $exhibtions = Adminex::all();
        $exhibtions = DB::select('select * from adminexes ');

        return view('Adminex.index',['exhibtions'=>$exhibtions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Adminex.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $admain = new Adminex;

        $admain->Exhibition_Name = $request->Exhibition_Name;
        $admain->Type_of_Exhibition = $request->Type_of_Exhibition;
        $admain->Number_of_Attendees = $request->Number_of_Attendees;
        $admain->Number_of_Exhibiting_Companies = $request->Number_of_Exhibiting_Companies;
        $admain ->Event_Budget = $request->Event_Budget;
        $admain ->state = $request->state;
        $admain ->photo = $request->photo;


        $imageName = $request->Exhibition_Name . '.' . request()->photo->getClientOriginalExtension();
        request()->photo->move(public_path('Exhibition'), $imageName);


        $admain->save();

        return redirect()->route('Adminex.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Adminex  $adminex
     * @return \Illuminate\Http\Response
     */
    public function show()
    {    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Adminex  $adminex
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adminex = Adminex::find($id);

        return view('Adminex.edit', compact('adminex'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Adminex  $adminex
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request -> validate([        
        "Exhibition_Name"=>"required:exhibtions,Exhibition_Name",
        "Type_of_Exhibition"=>"sometimes:exhibtions,Type_of_Exhibition",
        "Number_of_Attendees"=>"sometimes:exhibtions,Number_of_Attendees",
        "Number_of_Exhibiting_Companies"=>"sometimes:exhibtions,Number_of_Exhibiting_Companies",
        "Event_Budget"=>"sometimes:exhibtions,Event_Budget",
        "state"=>"sometimes:exhibtions,state",
        ]);
      $admain = Adminex::find($id);
      $admain->Exhibition_Name = $request->get('Exhibition_Name');
      $admain->Type_of_Exhibition = $request->get('Type_of_Exhibition');
      $admain->Number_of_Attendees = $request->get('Number_of_Attendees');
      $admain->Number_of_Exhibiting_Companies = $request->get('Number_of_Exhibiting_Companies');
      $admain->Event_Budget = $request->get('Event_Budget');
      $admain->state = $request->get('state');
      $admain->save();
        
  
        return redirect()->route('Adminex.index');   
         }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Adminex  $adminex
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Adminex::findOrFail($id);

       $blog->delete();

       return redirect()->route('Adminex.index');
    }
}
