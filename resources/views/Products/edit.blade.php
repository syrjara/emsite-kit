<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Edit</title>
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/FPE-Gentella-form-elements-1.css">
    <link rel="stylesheet" href="/assets/css/FPE-Gentella-form-elements.css">
    <link rel="stylesheet" href="/assets/css/Pretty-Registration-Form.css">
    <link rel="stylesheet" href="/assets/css/Pretty-Search-Form.css">
    <link rel="stylesheet" href="/assets/css/styles.css">
</head>

<body style="background: var(--bs-success);">
    <div class="row register-form">
        <div class="col-md-8 offset-md-2">
            <form method="post" action="{{ route('Product.update', $product->id) }}" class="custom-form" >
                @method('PATCH')
                @csrf
                <h1 style="font-size: 58px;">Edit Form</h1>
                <div class="row form-group">
                    <div class="col-sm-4 label-column">
                        <label class="col-form-label">Brand </label></div>
                    <div class="col-sm-6 input-column">
                        <input class="form-control" name="Brand" type="text" value="{{$product->Brand}}"></div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-4 label-column"><label class="col-form-label" >Model</label></div>
                    <div class="col-sm-6 input-column"><input class="form-control" name="Model" type="text" value="{{$product->Model}}"></div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-4 label-column">
                        <label class="col-form-label" >Warranty</label></div>
                    <div class="col-sm-6 input-column">
                        <input class="form-control" name="Warranty" type="text" value="{{$product->Warranty}}" ></div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-4 label-column"><label class="col-form-label" >Description</label></div>
                    <div class="col-sm-6 input-column" style="width: 292px;height: 65px;">
                        <input class="form-control" name="Description" type="text" style="height: 63px;" value="{{$product->Description}}"></div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-4 label-column"><label class="col-form-label" >Price</label></div>
                    <div class="col-sm-6 input-column">
                        <input class="form-control" type="number" name="Price" value="{{$product->Price}}"></div>
                </div>
                <button class="btn btn-light submit-button" type="Submit" value="edit">Submit&nbsp;</button>
            </form>
        </div>
    </div>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>