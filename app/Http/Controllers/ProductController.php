<?php

namespace App\Http\Controllers;

use App\Merch;
use App\Product;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $adminex = DB::select('select * from adminexes ');

        $categories = DB::select('select * from categories ');


        if(Auth::check() == false)
            return view('login');

        $merch = Merch::where('Email' , Auth::user()->email )->first();
        $products = Product::where('merch_id' , $merch->id)->get();

        return view('Products.index',compact('products'),['adminex'=>$adminex, 'categories'=>$categories, 'products'=>$products  , 'merch'=>$merch]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        if($request->search){
        
        $search = $request->get('search');
        $products = DB::table('products')->where('Brand', 'like' , '%' . $search . '%')->paginate(5);
        return view('Products.create',['products'=>$products]);
        }

        $products = DB::select('select * from products WHERE merch_id = ' . $request->id);


        return view('Products.create', ['products'=>$products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request -> validate([

        "Brand"=>"required:products,Brand",
        "Model"=>"required:products,Model",
        "Price"=>"required:products,Price",
        "Warranty"=>"required:products,Warranty",
        "Description"=>"required:products,Description",
        "image1"=>"required:products,image1",
        "image2"=>"required:products,image2",
        "image3"=>"required:products,image3",
        "image4"=>"required:products,image4",

        ]);
        $imageName1 = $request->Model . '1' .'.' .request()->image1->getClientOriginalExtension();
        request()->image1->move(public_path('ProductsImage'), $imageName1);

        $imageName2 = $request->Model  . '2' .  '.' .request()->image2->getClientOriginalExtension();
        request()->image2->move(public_path('ProductsImage'), $imageName2);

        $imageName3 = $request->Model . '3'. '.' .request()->image3->getClientOriginalExtension();
        request()->image3->move(public_path('ProductsImage'), $imageName3);

        $imageName4 = $request->Model.'4' .  '.' .request()->image4->getClientOriginalExtension();
        request()->image4->move(public_path('ProductsImage'), $imageName4);

        $data = $request->all();

        $data['merch_id'] = Merch::where('Email' , Auth::user()->email)->first()->id;

        Product::create($data);
        session()->flash('success',__('success'));
        return redirect()->route('Product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        return view('Products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
      $admain = Product::find($id);
      $admain->Brand = $request->get('Brand');
      $admain->Model = $request->get('Model');
      $admain->Price = $request->get('Price');
      $admain->Warranty = $request->get('Warranty');
      $admain->Description = $request->get('Description');
      $admain->save();
        
  
        return redirect()->route('Product.index');   
         }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $blog = Product::findOrFail($id);

        $blog->delete();

        return redirect()->route('Product.index');
    }
}
