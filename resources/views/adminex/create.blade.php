<!DOCTYPE html>
<html style="width: 100%;">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Home - EM-S-ITE</title>
    <link rel="stylesheet" href="/assets/css/Responsive-footer.css">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter:400,700">
    <link rel="stylesheet" href="/assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/fonts/material-icons.min.css">
    <link rel="stylesheet" href="/assets/fonts/typicons.min.css">
    <link rel="stylesheet" href="/assets/css/--mp---Masonry-Gallery-with-Loader.css">
    <link rel="stylesheet" href="/assets/css/-Filterable-Gallery-with-Lightbox-BS4-.css">
    <link rel="stylesheet" href="/assets/css/Bootstrap-Image-Uploader.css">
    <link rel="stylesheet" href="/assets/css/Drag--Drop-Upload-Form.css">
    <link rel="stylesheet" href="/assets/css/Drag-Drop-File-Input-Upload.css">
    <link rel="stylesheet" href="/assets/css/gradient-navbar-1.css">
    <link rel="stylesheet" href="/assets/css/gradient-navbar.css">
    <link rel="stylesheet" href="/assets/css/Header-Dark.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.css">
    <link rel="stylesheet" href="/assets/css/Image_center.css">
    <link rel="stylesheet" href="/assets/css/Image-Tab-Gallery-Vertical.css">
    <link rel="stylesheet" href="/assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="/assets/css/Map-Clean.css">
    <link rel="stylesheet" href="/assets/css/Masonry-gallery-cards-responsive.css">
    <link rel="stylesheet" href="/assets/css/Panel-Element-1.css">
    <link rel="stylesheet" href="/assets/css/Panel-Element-2.css">
    <link rel="stylesheet" href="/assets/css/Panel-Element-3.css">
    <link rel="stylesheet" href="/assets/css/Panel-Element.css">
    <link rel="stylesheet" href="/assets/css/Projects-Clean.css">
    <link rel="stylesheet" href="/assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="/assets/css/Simple-Side-Nav.css">
    <link rel="stylesheet" href="/assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="/assets/css/vanilla-zoom.min.css">
    <link rel="icon" href="/assets/img/logo.png" type="image/x-icon">

</head>

<body style="width: 100%;">
   @extends('layou.app')
@section('content')
    <main class="page landing-page" style="width: 100%;">

        <section class="clean-block clean-info dark" style="width: 95%;background: rgb(255,255,255);padding-top: 1%;padding-left: 25%;padding-top: 70px; direction: center">
                    <form action="{{route('Adminex.store')}}" method="post" class="border rounded-0 shadow-lg"  data-aos="zoom-in" data-aos-duration="500" data-aos-delay="50" style="background: var(--bs-light);padding: 20px;width: 500px;margin: 40px 100px;" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                        <img data-aos="zoom-in-up" src="/assets/img/f29a1ebaf6ba41c8871ee1ace3098555.png" style="padding: 0px;margin: 0% 28%;font-size: 20px;">
                            <div class="col">
                                <div class="mb-3">
                                    <div>
                                    <input class="form-control" type="text" id="dealername" placeholder="Exhibition_Name" name="Exhibition_Name" required>
                                    </div>
                                    </div>
                                </div>
                            </div>
            <div class="files color form-group mb-3"><label class="form-label" style="color: var(--bs-green);">
                <strong> @lang('The.Upload Exhibtion image') </strong>
            </label>
            <input type="file" name="photo" class="form-control" multiple accept=".png, .jpg, .jpeg" style="width: auto;height: auto;margin: 0px;padding: 121px 0px 85px 118.1px;"
                />
            </div>
                        <div class="col" style="padding: 27px;">
                            <div class="row">
                                <div class="col" name="Type_of_Exhibition">
                                    <label class="col-form-label" name="Type_of_Exhibition">@lang('The.Type of Exhibition')<br>
                                </label>
                            </div>
                            <div>
                            <select class="form-select" onchange="myFunction(event)" name="Type_of_Exhibition">
                                    <option name="Sٍٍpecific">@lang('The.Sٍٍpecific')</option>
                                    <option name="inclusive">@lang('The.inclusive')</option>
                                </select>
                                </div>
                                <div class="col" name="Number_of_Attendees">
                                    <label class="col-form-label" name="Number_of_Attendees">@lang('The.Number of Attendees')<br>
                                    </label>
                                </div>
                                <div>
                                <select class="form-select" name="Number_of_Attendees">
                                    <option name="Less than 1K">@lang('The.Less than 1K')</option>
                                    <option name="1 K - 10 K">1 K - 10 K </option>
                                    <option name="10 K +">10 K +</option>
                                </select>
                                </div>
                                <div class="col" name="Number_of_Exhibiting_Companies">
                                    <label class="col-form-label" name="Number_of_Exhibiting_Companies">@lang('The.Number of Exhibiting Companies')<br>
                                    </label>
                                </div>
                                <div>
                                <select class="form-select" name="Number_of_Exhibiting_Companies">
                                    <option name="None">@lang('The.None')</option>
                                    <option name="Less than 50">@lang('The.Less than 50')</option>
                                    <option name="50+">50+</option>
                                </select>
                                </div>
                                <div class="col" name="Event_Budget">
                                    <label class="col-form-label" name="Event_Budget">@lang('The.Event Budget')<br>
                                    </label>
                                </div>
                                <div>
                                <select class="form-select" name="Event_Budget">
                                    <option name="Less Than 10 M">@lang('The.Less Than 10 M')</option>
                                    <option name="10 M - 100 M">10 M - 100 M</option>
                                    <option name="100 M - 1 B">100 M - 1 B</option>
                                </select>
                                </div>
                                <div class="col" name="state">
                                    <label class="col-form-label" name="state"> @lang('The.state') <br>
                                    </label>
                                </div>
                                <div>
                                <select class="form-select" name="state">
                                    <option name="active">@lang('The.active')</option>
                                    <option name="finish">@lang('The.finish')</option>
                                </select>
                                </div>
                                <!-- // -->
                            </div>
                        </div>
                        <div id="signup" class="mb-3">
                            <input type="submit" class="btn btn-primary d-block w-100" type="submit" style="background: var(--bs-green);" value="send">
                        </div>
                    </form>
                
        </section>
    </main>
    
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdn.rawgit.com/digistate/resouces/master/multipleFilterMasonry.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script src="https://cdn.rawgit.com/desandro/masonry/master/dist/masonry.pkgd.min.js"></script>
    <script src="/assets/js/vanilla-zoom.js"></script>
    <script src="/assets/js/--mp---Masonry-Gallery-with-Loader.js"></script>
    <script src="/assets/js/-Filterable-Gallery-with-Lightbox-BS4-.js"></script>
    <script src="/assets/js/theme.js"></script>
    <script src="/assets/js/Bootstrap-Image-Uploader.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.js"></script>
    <script src="/assets/js/Image-Tab-Gallery-Vertical.js"></script>
    <script src="/assets/js/Panel-Element-1.js"></script>
    <script src="/assets/js/Panel-Element.js"></script>
    <script src="/assets/js/selectex.js"></script>
    <script src="/assets/js/Simple-Side-Nav.js"></script>
    <script src="/assets/js/Simple-Slider.js"></script>
    <script async type="text/javascript" src="https://userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/0b51cec2d899412ea501d9269bffcda03dbf932c25e343a4b206a355923ba199.js"></script>
</body>

@endsection