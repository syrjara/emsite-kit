<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buy extends Model
{
    protected $fillable = [ 'Full_Name', 'Mobile_Number', 'email', 'Number', 'City', 'Town', 'Comments'];
}
