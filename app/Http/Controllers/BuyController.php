<?php

namespace App\Http\Controllers;

use App\Buy;
use Illuminate\Http\Request;
USE DB;


class BuyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = DB::select("select * from products WHERE id = " . $request->id);
        
        return view('Buy.index' , ['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->search){
        
        $search = $request->get('search');
        $buy = DB::table('buys')->where('Full_Name', 'like' , '%' . $search . '%')->paginate(5);
        return view('Buy.create',['buy'=>$buy]);
        }
        $buy = Buy::all();
        $buy = DB::select("select * from buys");
        return view('Buy.create' , ['buy'=>$buy]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $buy = new Buy;

        $buy->Full_Name = $request->Full_Name;
        $buy->Mobile_Number = $request->Mobile_Number;
        $buy->email = $request->email;
        $buy->Number = $request->Number;
        $buy->City = $request->City;
        $buy->Town = $request->Town;
        $buy->Comments = $request->Comments;

        $buy->save();

        return redirect()->route('Admin.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Buy  $buy
     * @return \Illuminate\Http\Response
     */
    public function show(Buy $buy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Buy  $buy
     * @return \Illuminate\Http\Response
     */
    public function edit(Buy $buy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Buy  $buy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Buy $buy)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Buy  $buy
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Buy::findOrFail($id);

       $blog->delete();

       return redirect()->route('Buy.create');
    }
}
